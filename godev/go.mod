module golang.org/x/telemetry/godev

go 1.20

require (
	cloud.google.com/go/cloudtasks v1.10.0
	cloud.google.com/go/storage v1.30.1
	github.com/evanw/esbuild v0.17.19
	github.com/google/go-cmp v0.5.9
	github.com/yuin/goldmark v1.5.4
	golang.org/x/exp v0.0.0-20230817173708-d852ddb80c63
	golang.org/x/mod v0.13.0
	golang.org/x/telemetry v0.0.0-00010101000000-000000000000
	google.golang.org/api v0.122.0
)

require (
	cloud.google.com/go v0.110.0 // indirect
	cloud.google.com/go/compute v1.19.0 // indirect
	cloud.google.com/go/compute/metadata v0.2.3 // indirect
	cloud.google.com/go/iam v1.0.1 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/s2a-go v0.1.3 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/googleapis/enterprise-certificate-proxy v0.2.3 // indirect
	github.com/googleapis/gax-go/v2 v2.8.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	go.opencensus.io v0.24.0 // indirect
	golang.org/x/crypto v0.7.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/oauth2 v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	golang.org/x/xerrors v0.0.0-20220907171357-04be3eba64a2 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20230410155749-daa745c078e1 // indirect
	google.golang.org/grpc v1.55.0 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

require (
	github.com/yuin/goldmark-meta v1.1.0
	golang.org/x/sys v0.13.0 // indirect
)

replace golang.org/x/telemetry => ./..
